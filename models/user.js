'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    email: {
      type:DataTypes.STRING,
      validate: {
        isEmail: true
      }
    },
    api_token: DataTypes.TEXT,
    access_token: DataTypes.TEXT,
    refresh_token: DataTypes.TEXT,
    created_at: DataTypes.INTEGER,
    updated_at: DataTypes.INTEGER

  }, 
  {
    underscored: true,
    freezeTableName: true,
    tableName: 'user'
  }, 
  {
    classMethods: {
      associate: (models) => {
        // associations can be defined here
      }
    }
  });
  return User;
};