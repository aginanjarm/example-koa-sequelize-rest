'use strict';

const booking = require("../../models/user");

const index = async ctx => {
  try {
    // const bookings = await knex("bookings").select();
    ctx.body = {
      data: {
          "id": "uuid01",
          "name": "ahmad"
      }
    };
  } catch (error) {
    console.error(error);
  }
};

// const show = async ctx => {
//   try {
//     const { id } = ctx.params;
//     const article = await knex("bookings")
//       .select()
//       .where({ id });
//     if (!article.length) {
//       throw new Error("The requested resource does not exists");
//     }
//     ctx.body = {
//       data: article
//     };
//   } catch (error) {
//     ctx.status = 404;
//     ctx.body = {
//       error: error.message
//     };
//   }
// };

// const create = async ctx => {
//   try {
//     const { body } = ctx.request;
//     const article = await knex("bookings").insert(body);
//     if (!article.length) {
//       throw new Error("The resource already exists");
//     }
//     ctx.status = 201;
//     ctx.set("Location", `${ctx.request.URL}/${article[0]}`);
//     ctx.body = {
//       data: article
//     };
//   } catch (error) {
//     ctx.status = 409;
//     ctx.body = {
//       error: "The resource already exists"
//     };
//   }
// };

module.exports = {  
    index
};
