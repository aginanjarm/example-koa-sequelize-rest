const jwt = require('jwt-simple')
const Model = require('../../models')
const User = Model.user

const generateJwt = (user) => {
    let timestamp = new Date().getTime()
    return jwt.encode({ sub: user.id, iat: timestamp }, process.env.APP_KEY)
}

module.exports = {
    signup: async (ctx, callback) => {
        let data = JSON.parse(ctx.request.rawBody)
        var result = false, errMessage
        
        await User.create({
            first_name: data.first_name,
            last_name: data.last_name,
            email: data.email
        }).then(function (user) {
            result = true
            callback(result)
        })
        .catch(function (err) {
            let error = err.errors
            error.forEach(element => {
                errMessage = element.message
            });
            callback(result, errMessage)
        })
        
    },

    signin: function (req, res, next) {
        res.send({ token: tokenForUser(req.user), email: req.user.email })
    }
}