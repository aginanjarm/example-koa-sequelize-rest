require('dotenv').config();

const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const apiRoutes = require('./routes/api.routes');

const app = new Koa();
const PORT = process.env.PORT || 3000;

// sessions
const session = require('koa-session')
app.keys = [process.env.APP_KEY]

// body parser
app.use(bodyParser({
  onerror: function (err, ctx) {
    ctx.throw('body parse error', 422);
  }
}))

// authentication
require('./auth')
const passport = require('koa-passport');
app.use(passport.initialize());
app.use(apiRoutes.routes()).use(apiRoutes.allowedMethods())

// Require authentication for now
app.use(function(ctx, next) {
  if (ctx.isAuthenticated()) {
    return next()
  } else {
    ctx.redirect('/')
  }
})

const server = app.listen(PORT, ()=>
    console.log('Koa Server on ', PORT)
  ).on('error', err => {
    console.error(err);
});

module.exports = server;