const Router = require("koa-router")
const route = new Router()
const Auth = require("../src/controllers/authentication")
const bookingsController = require("../src/controllers/bookings.controller")
const BASE_URL = `/api/v1/bookings`
const passport = require('koa-passport')
const fs = require('fs')

// 
// route.get('/', function(ctx) {
//   ctx.type = 'html'
//   ctx.body = fs.createReadStream('views/login.html')
// })
route.get('/', (ctx)=>{
  ctx.body = {
    success: true,
    code: 200,
    data: {},
    message: 'Welcome, selamat datang, wilujeung sumping'
  }
})


// Registration 
route.post('/registration', (ctx) => {
  return Auth.signup(ctx, (result, error) => {
    if(result) 
    {
      ctx.body = {
        success: true,
        code: 200,
        data: ctx.request.body,
        message: 'Registrasi sukses.'
      }
    } else {
      let errMessage = (typeof error === 'undefined') ? 'Bad request.' : error
      ctx.body = {
        success: false,
        code: 466,
        message: errMessage
      }
    }

    return ctx.body
  })
})

// POST /login
route.post('/login',function(ctx) {
  return passport.authenticate('local', function(err, user, info, status) {
    if (user === false) {
      ctx.body = { success: false, code: 401 }
    } else {
      ctx.body = { success: true, user: user }
    }
  })(ctx)
})

route.get('/logout', function(ctx) {
  ctx.logout()
  ctx.redirect('/')
})

route.get('/auth/facebook',
  passport.authenticate('facebook')
)

route.get('/auth/facebook/callback',
  passport.authenticate('facebook', {
      successRedirect: '/app',
      failureRedirect: '/'
  })
)

route.get('/auth/twitter',
  passport.authenticate('twitter')
)

route.get('/auth/twitter/callback',
  passport.authenticate('twitter', {
      successRedirect: '/app',
      failureRedirect: '/'
  })
)

route.get('/auth/google',
  passport.authenticate('google')
)

route.get('/auth/google/callback',
  passport.authenticate('google', {
      successRedirect: '/app',
      failureRedirect: '/'
  })
)

route.get('/app', function(ctx) {
  ctx.type = 'html'
  ctx.body = fs.createReadStream('views/app.html')
})


// Booking
route.get(`${BASE_URL}`, bookingsController.index)

// Custom
route.post('/custom', function (ctx) {
  return passport.authenticate('local', function (err, user, info, status) {
    if (user === false) {
      ctx.body = { success: false }
      ctx.throw(401)
    } else {
      ctx.body = { success: true }
      return ctx.login(user)
    }
  })(ctx)
})

// Export
module.exports = route